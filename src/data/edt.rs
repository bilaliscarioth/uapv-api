use super::json::SubjectJson;
use chrono::DateTime;
use chrono_tz::Europe::Paris;
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;
use std::default::Default;

#[derive(Debug, Serialize, Deserialize)]
pub enum TimetableFilters {
    OnlyHolidays,
    Name(String),
    Teacher(String),
    Type(String),
    None,
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct Subject {
    pub name: String,
    pub start_at: String,
    pub end_at: String,
    pub room: String,
    pub types: String, // CM, TD, TP, Eval...
    pub teacher: String,
}

impl Subject {
    /// Parsing JSON into HashMap <Date, Timetable>
    pub fn import<'a>(data: &'a [SubjectJson]) -> BTreeMap<String, Vec<Subject>> {
        let mut timetable: BTreeMap<String, Vec<Subject>> = BTreeMap::new();

        for hour in data.iter() {
            if hour.title.contains("Annulation") {
                continue;
            }

            let start = DateTime::parse_from_rfc3339(&hour.begin_at)
                .unwrap()
                .with_timezone(&Paris);

            let date = start.date_naive().to_string();

            let end = DateTime::parse_from_rfc3339(&hour.end_at)
                .unwrap()
                .with_timezone(&Paris);

            let timetable_current_day = match timetable.get_mut(&date) {
                Some(find) => find,
                None => {
                    timetable.insert(date.clone(), Vec::new());
                    timetable.get_mut(&date).unwrap()
                }
            };

            let mut info: Vec<&str> = hour.title.split("\n").collect();
            if info.len() > 1 {
                info.pop();
            }

            let mut current_subject = Subject::default();
            current_subject.start_at = start.to_string();
            current_subject.end_at = end.to_string();

            current_subject.name = info[0].to_string();

            if info.len() >= 4 {
                if info.len() == 4 {
                    current_subject.room = String::from("Cours non mise en place....");
                }
                current_subject.teacher = info[1].to_string();
                current_subject.types = hour.theme.to_string();
            }

            if info.len() >= 5 {
                current_subject.room = info[info.len() - 2].to_string();
            }

            timetable_current_day.push(current_subject);
        }

        timetable
    }
}
