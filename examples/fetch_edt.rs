use uapv_api;
use uapv_api::service::edt::*;

pub fn main() {
    let mut uapv_id = "ID".to_string();
    let mut uapv_pass = "PASSWORD".to_string();

    let edt_session = uapv_api::login::<EdtSession>(&mut uapv_id, &mut uapv_pass).unwrap();

    let t = edt_session.get_timetable(EdtTimetable::Mine);
    let c = edt_session.get_code(EdtCode::Teachers);
    let z = edt_session.get_free_room(EdtFreeRoomSite::Ceri, "2024-11-12", "8.5", "3");

    println!("{:?}", t);
    println!("{:?}", c);
    println!("{:?}", z);
    println!("{:?}", edt_session);

    uapv_api::logout::<EdtSession>(&edt_session);
    println!("{:?}", edt_session); // edt_session.token will be clear.
}
