#![forbid(unsafe_code)]
pub mod data;
pub mod service;

use ureq::Error;

pub enum Service {
    EDT,
    ADMIN,
    PARTAGE,
}

pub trait UAPVService {
    fn login(ticket: String) -> Result<Box<Self>, String>;
    fn get_link() -> String;
    fn logout(&self);
    fn still_login(&self) -> Result<(), String>;
}

pub fn login<T: UAPVService>(
    username: &mut String,
    password: &mut String,
) -> Result<Box<T>, String> {
    let mut res = ureq::get(
        "https://cas.univ-avignon.fr/cas/login?service=https://edt.univ-avignon.fr/login",
    )
    .call()
    .expect("Can't send any request");

    let mut body = res.into_string().expect("Size is greater than 10M...");

    match body.find("name=\"execution\" value=\"") {
        Some(index) => body = body.get(index + 24..).unwrap().to_string(),
        None => return Err("Not yet implemented...".to_string()),
    }

    match body.find("\"") {
        Some(index) => body = body.get(..index).unwrap().to_string(),
        None => return Err("Not yet implemented...".to_string()),
    }

    let req = ureq::post(
        "https://cas.univ-avignon.fr/cas/login?service=https://edt.univ-avignon.fr/login",
    )
    .send_form(&[
        ("username", username),
        ("password", password),
        ("execution", &body),
        ("_eventId", "submit"),
        ("geolocation", ""),
    ]);

    if let Err(Error::Status(code, r)) = req {
        println!("DEBUG {:?}", r);
        return match code {
            401 => Err("Can't login with this credentials...".into()),
            500 => Err("API-EDT is not available right now...".into()),
            _ => Err("Another HTTP Error to describe?".into()),
        };
    }

    res = req.unwrap();

    // At the end of login, remove credentials.
    *username = String::from("");
    *password = String::from("");

    let url = res.get_url().to_string();

    if !url.contains("ticket=") {
        return Err("Can't find the ticket pass to login...".to_string());
    }

    _ = ureq::get(res.get_url());

    let ticket_index = url.find("ticket=").unwrap();
    let ticket = url.get(ticket_index + 7..).unwrap().to_string();

    Ok(T::login(ticket).unwrap())
}

pub fn logout<T: UAPVService>(session: &T) {
    session.logout();
}
