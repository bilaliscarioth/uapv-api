use serde::{Deserialize, Serialize};
use std::fmt::Debug;
use std::vec::Vec;

#[derive(Debug, Deserialize, Serialize)]
pub struct EntUserInfo {
    #[serde(rename = "mel")]
    pub mail: String,
    pub uid: String,
    #[serde(rename = "dateNaissance")]
    pub born_date: String,
    #[serde(rename = "prenom")]
    pub name: String,
    #[serde(rename = "nom")]
    pub surname: String,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct ListJson<T: Debug> {
    pub results: Vec<T>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SubjectJson {
    #[serde(rename = "start")]
    pub begin_at: String,
    #[serde(rename = "end")]
    pub end_at: String,
    #[serde(rename = "type")]
    pub theme: String, // CM, TD, TD, Evaluation
    pub title: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ElementsJson {
    letter: Option<String>,
    name: Option<Vec<ReceivedJson>>,
    names: Option<Vec<ReceivedJson>>,
}

/// name of the teachers, rooms
/// Code for Formation and rooms or (teacher but this is not usable)
/// UAPV_RH si code for teachers !
#[derive(Debug, Deserialize, Serialize)]
pub struct ReceivedJson {
    name: String,
    #[serde(rename = "uapvRH")]
    uapv_rh: Option<String>,
    code: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct FreeRoom {
    libelle: String,
    capacite: String,
}
