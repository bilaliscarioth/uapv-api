use crate::data::edt::Subject;
use crate::data::json::*;
use crate::UAPVService;
use serde_json;
use std::cell::RefCell;
use std::collections::BTreeMap;
use std::error::Error;
use std::fmt::Display;
use ureq;

#[derive(Debug)]
pub struct EdtSession {
    token: RefCell<String>,
}

pub enum EdtCode {
    Ueos,
    Rooms,
    Subjects,
    Teachers,
    Formations,
}

impl Display for EdtCode {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "https://edt-api.univ-avignon.fr/app.php/api{}",
            match self {
                EdtCode::Teachers => "/enseignants/",
                EdtCode::Subjects => "/UE",
                EdtCode::Ueos => "/UEO",
                EdtCode::Formations => "/elements",
                EdtCode::Rooms => "/salles",
            }
        )
    }
}

pub enum EdtTimetable {
    Mine,
    Teacher(&'static str),
    Ueo(&'static str),
    Subject(&'static str),
    Formation(&'static str),
    Room(&'static str),
}

impl Display for EdtTimetable {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "https://edt-api.univ-avignon.fr/app.php/api{}",
            match self {
                EdtTimetable::Mine => "/events_perso?autre=false".to_string(),
                EdtTimetable::Teacher(t) => "/events_enseignant/".to_owned() + t,
                EdtTimetable::Subject(s) | EdtTimetable::Ueo(s) =>
                    "/events_matieres/".to_owned() + s,
                EdtTimetable::Formation(f) => "/events_promotion/".to_owned() + f,
                EdtTimetable::Room(r) => "/events_salle/".to_owned() + r,
            }
        )
    }
}

pub enum EdtFreeRoomSite {
    Ceri,
    HannahArendt,
    IUT,
    Agroscience,
}

impl Display for EdtFreeRoomSite {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                EdtFreeRoomSite::Ceri => "CERI",
                EdtFreeRoomSite::HannahArendt => "CV",
                EdtFreeRoomSite::IUT => "IUT",
                EdtFreeRoomSite::Agroscience => "AGR",
            }
        )
    }
}

impl UAPVService for EdtSession {
    fn get_link() -> String {
        String::from("https://edt-api.univ-avignon.fr")
    }

    fn login(ticket: String) -> Result<Box<EdtSession>, String> {
        _ = ureq::get(format!("https://edt.univ-avignon.fr/login?ticket={}", ticket).as_str())
            .call();

        let body = format!("{{ \"ticket\": \"{}\", \"service\": \"https://edt.univ-avignon.fr\", \"version\": \"\" }}", ticket);

        let req = ureq::post("https://edt-api.univ-avignon.fr/app.php/api/auth/login")
            .set("Origin", "https://edt.univ-avignon.fr")
            .send_string(&body);

        if let Err(r) = req {
            println!("{:?}", r);
            return Err("not yet implemented...".to_string());
        }

        let res = req.unwrap();
        let mut token = String::new();

        if let Ok(mut data) = res.into_string() {
            //Refactor this shit...
            let index = data.find("token\":").unwrap();
            data = data.get(index + 8..).unwrap().to_string();
            data = data.get(..data.find("\"").unwrap()).unwrap().to_string();
            token = data;
        }

        if token.is_empty() {
            return Err("No token received from edt-api.univ-avignon.fr".to_string());
        }

        Ok(Box::new(EdtSession {
            token: RefCell::new(token),
        }))
    }

    fn still_login(&self) -> Result<(), String> {
        let req = ureq::get("https://edt-api.univ-avignon.fr/api/application/ping")
            .set("Origin", "https://edt.univ-avignon.fr")
            .set("token", &self.token.clone().into_inner())
            .call();

        if let Err(ureq::Error::Status(s, r)) = req {
            if s == 401 {
                return Err("Expired session...".to_string());
            }
            return Err(format!(" {s} -  {:?} another scenario ?? ", r));
        }

        Ok(())
    }

    fn logout(&self) {
        self.still_login().unwrap();

        _ = ureq::post("https://edt-api.univ-avignon.fr/api/auth/logout")
            .set("Origin", "https://edt.univ-avignon.fr")
            .set("token", &self.token.clone().into_inner());

        *self.token.borrow_mut() = String::new();
    }
}

impl EdtSession {
    fn fetch(&self, url: String) -> Result<String, Box<dyn Error>> {
        self.still_login().unwrap();

        let req = ureq::get(url.as_str())
            .set("Origin", "https://edt.univ-avignon.fr/")
            .set("token", &self.token.clone().into_inner())
            .call();

        if let Err(ref r) = req {
            return Err(format!("{:?}", r).into());
        }

        let res = req.unwrap();

        match res.into_string() {
            Ok(r) => Ok(r),
            Err(_) => {
                println!("Not implemented yet...");
                Err("eee".into())
            }
        }
    }

    pub fn get_timetable(
        &self,
        timetable: EdtTimetable,
    ) -> Result<BTreeMap<String, Vec<Subject>>, String> {
        //! get_timetable(EdtTimetable::Mine), will get the current timetable.
        //! fetch Teacher's timetable be sure to get their uapvRh code with
        //! EdtSession::get_code()
        //! Education, Subjects, Room follow the same rules...
        //! Otherwhise you will only get a 404.

        let data = self.fetch(format!("{timetable}")).unwrap();
        let list_subject = serde_json::from_str::<ListJson<SubjectJson>>(&data).unwrap();

        Ok(Subject::import(&list_subject.results))
    }

    pub fn get_code(&self, code: EdtCode) -> Result<ListJson<ElementsJson>, String> {
        //! Code will give you the id needed for fetching Teacher/Subjects/Rooms
        let data = self.fetch(format!("{code}")).unwrap();
        Ok(serde_json::from_str::<ListJson<ElementsJson>>(&data).unwrap())
    }

    pub fn get_free_room(
        &self,
        site: EdtFreeRoomSite,
        date: &'static str,
        from: &'static str,
        elapse: &'static str,
    ) -> Result<ListJson<FreeRoom>, String> {
        //! How weird is backend of edt-api... \n
        //! You should never pair HannahArendt/Agroscience and elapse (with) seconds. \n
        //! date follow this format `YYYY-MM-DD` \n
        //! from & elapse in hour 0.5 -> 00h30min \n
        let data = self.fetch(format!("https://edt-api.univ-avignon.fr/api/salles/disponibilite?site={site}&duree={elapse}&debut={from}&date={date}")).unwrap();
        println!("{:?}", data);
        Ok(serde_json::from_str::<ListJson<FreeRoom>>(&data).unwrap())
    }
}
